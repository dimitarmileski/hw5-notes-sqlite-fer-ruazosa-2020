package hr.fer.ruazosa.noteapplication

import android.arch.lifecycle.ViewModelProvider
import android.arch.persistence.room.Room
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_notes_details.*
import java.util.*

class NotesDetails : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notes_details)

        val viewModel =
            ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(application)).get(
                NotesViewModel::class.java)

        var noteTitle = findViewById<EditText>(R.id.noteTitleEditText)
        var noteDesc = findViewById<EditText>(R.id.noteDescriptionEditText)

        if(NoteSinglenton.nodeIndex == -1){
            saveNoteButton.setText("Save Note")
        }else{
            saveNoteButton.setText("Update Note")
        }
        noteTitle.setText(NoteSinglenton.noteTitle)
        noteDesc.setText(NoteSinglenton.noteDescription)

        saveNoteButton.setOnClickListener {
            if(NoteSinglenton.nodeIndex == -1){
                val dbNote = Note(
                    noteTitle =noteTitleEditText.text.toString(),
                    noteDescription = noteDescriptionEditText.text.toString(),
                    noteDate = Date().toString() )

                Thread(Runnable {
                    DatabaseSinglenton.db?.noteDao()?.insertAll(dbNote)
                    runOnUiThread {
                        noteTitleEditText.text = null
                        noteDescriptionEditText.text = null

                    }
                }).start()


            }else{
                Thread(Runnable {
                    var tempNote = DatabaseSinglenton.db?.noteDao()?.findByName(NoteSinglenton.noteTitle)
                    DatabaseSinglenton.db?.noteDao()?.update(noteTitle.text.toString(),
                        noteDesc.text.toString(),
                        tempNote?.uid
                    )
                }).start()

            }

            startActivity(Intent(this, MainActivity::class.java))
        }
    }
}
