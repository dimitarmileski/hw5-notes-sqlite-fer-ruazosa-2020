package hr.fer.ruazosa.noteapplication

import android.arch.lifecycle.ViewModel
import kotlin.concurrent.thread

object DatabaseSinglenton {
    var db: NoteDatabase? = null

    @Synchronized fun populateViewModel(viewModel: NotesViewModel){
        var tempNotes: List<Note>? = null
        thread {
            tempNotes = db?.noteDao()?.getAll()
        }
        viewModel.notesList.value = tempNotes
    }
}