package hr.fer.ruazosa.noteapplication

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.util.Date
@Entity
data class Note(
    @PrimaryKey(autoGenerate = true) var uid: Int? = null,
    @ColumnInfo(name = "noteTitle") var noteTitle: String ? = null,
    @ColumnInfo(name = "noteDescription") var noteDescription: String ? = null,
    @ColumnInfo(name = "noteDate") var noteDate: String? = null
)
