package hr.fer.ruazosa.noteapplication


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.persistence.room.Room
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.concurrent.thread


class MainActivity : AppCompatActivity(),NotesAdapter.OnNoteListener  {

    lateinit var notesAdapter: NotesAdapter
    lateinit var viewModel: NotesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        DatabaseSinglenton.db = Room.databaseBuilder(
                applicationContext,
                NoteDatabase::class.java, "database-name3"
            ).allowMainThreadQueries()
            .build()

        listOfNotesView.layoutManager = LinearLayoutManager(applicationContext)

        val decorator = DividerItemDecoration(applicationContext, LinearLayoutManager.VERTICAL)
        decorator.setDrawable(ContextCompat.getDrawable(applicationContext, R.drawable.cell_divider)!!)
        listOfNotesView.addItemDecoration(decorator)

        viewModel =
            ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(application)).get(
                NotesViewModel::class.java)

        DatabaseSinglenton.populateViewModel(viewModel)

        notesAdapter = NotesAdapter(viewModel, this)
        listOfNotesView.adapter = notesAdapter


        viewModel.notesList.observe(this, Observer {
            notesAdapter.notifyDataSetChanged()
        })

        newNoteActionButton.setOnClickListener {
            val intent = Intent(this, NotesDetails::class.java)
            NoteSinglenton.nodeIndex = -1
            NoteSinglenton.noteTitle = ""
            NoteSinglenton.noteDescription = ""
            startActivity(intent)
        }

    }

    override fun onResume() {
        super.onResume()
        notesAdapter.notifyDataSetChanged()
        viewModel.notesList.value = DatabaseSinglenton.db?.noteDao()?.getAll()
    }

    override fun onNoteClick(postion: Int) {
        val intent = Intent(this, NotesDetails::class.java)

        var note = viewModel.getNoteFromRepository(postion)

        NoteSinglenton.noteTitle = note?.noteTitle
        NoteSinglenton.noteDescription = note?.noteDescription
        NoteSinglenton.noteDate = note?.noteDate
        NoteSinglenton.nodeIndex = postion
        startActivityForResult(intent, 0)
    }

    override fun onDeleteClick(postion: Int) {
        val builder = AlertDialog.Builder(this@MainActivity)
        builder.setTitle("Delete Alert")
        builder.setMessage("Do you want to delete note ${viewModel.getNoteFromRepository(postion).noteTitle}  ?")

        builder.setPositiveButton("YES"){dialog, which ->

            var noteForDelete = viewModel.getNoteFromRepository(postion)

            Thread(Runnable {
                DatabaseSinglenton.db?.noteDao()?.delete(noteForDelete)
            }).start()

            val intent = Intent(
                this@MainActivity,
                MainActivity::class.java
            )
            startActivity(intent)
        }

        builder.setNeutralButton("Cancel"){_,_ ->

        }

        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

}
