package hr.fer.ruazosa.noteapplication

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

@Dao
interface NoteDao{
    @Query("SELECT * FROM note")
    fun getAll(): List<Note>

    @Query("SELECT * FROM note WHERE uid IN (:userIds)")
    fun loadAllByIds(userIds: IntArray): List<Note>

    @Query("SELECT * FROM note WHERE noteTitle LIKE :noteTitle" +
            " LIMIT 1")
    fun findByName(noteTitle: String?): Note

    @Insert
    fun insertAll(vararg users: Note)

    @Delete
    fun delete(note: Note)

    @Query("SELECT * FROM note WHERE uid = (SELECT max(uid) FROM note)")
    fun getLastSavedNote(): Note

    @Query("UPDATE note SET noteTitle = :noteTitle, noteDescription= :noteDescription WHERE uid =:uid")
    fun update(noteTitle: String?, noteDescription: String?, uid: Int?)
}